# Node Project

Projeto feito com NodeJs

-Mongoose

-Express

-Babel

-Sucrase

-Typescript

-nodemon

## Utilização

Inicie utilizando os seguintes comandos:

```
cd nodeproject
yarn
yarn dev
```

## Node Project

Um projeto back-end desenvolvido com NodeJs

## Description

Projeto de API utilizando ExpressJs com acesso a banco MongoDB através da ferramenta Mongoose.

## Usage

Rotas:

[/users](localhost:3333/users)

[/create](localhost:3333/create)

[/userFullName/:id](localhost:3333/userFullName/625710288adebfd2214e0362)

## Authors

-   Fábio Lima

## License

For open source projects, say how it is licensed.
