import { Response, Request } from 'express'
import User from '../schemas/Users'

class UserController {
    public async index(req: Request, res: Response): Promise<Response> {
        const users = await User.find()

        return res.json(users)
    }

    public async create(req: Request, res: Response): Promise<Response> {
        const user = await User.create(req.body)

        return res.json(user.firstName)
    }

    public async fullName(req: Request, res: Response): Promise<Response> {
        try {
            const user = await User.findOne({ _id: req.params.id })

            return res.json(user.fullName())
        } catch (ex) {
            return res.json(`Falha ${ex.message}`)
        }
    }
}

export default new UserController()
