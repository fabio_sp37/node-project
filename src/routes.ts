import { Router } from 'express'
import UserController from './controllers/UserController'

const routes = Router()

routes.get('/users', UserController.index)
routes.post('/userCreate', UserController.create)
routes.get('/userFullName/:id', UserController.fullName)

export default routes
